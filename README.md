# Serveur Zedou

Lien vers le serveur discord "Zedou" pour quartier du 12e, action portée par [le Relais 59](https://csrelais59.org).

## Status

[![build status]](https://gitlab.com/relais59/discord/badges/master/build.svg)
[![coverage report]](https://gitlab.com/relais59/discord/badges/master/coverage.svg)
